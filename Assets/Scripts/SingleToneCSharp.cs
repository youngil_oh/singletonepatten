using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingleTonePattern
{
    public class SingleToneCSharp
    {
        private static SingleToneCSharp instance = null;

        public static SingleToneCSharp Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new SingleToneCSharp();
                }
                return instance;
            }
        }

        private float randomNumber;

        private SingleToneCSharp()
        {
            randomNumber = Random.Range(0f, 1f);
        }

        public void TestSingleton()
        {
            Debug.Log($"Hello Singleton, random number is : {randomNumber}");
        }
    }
}

