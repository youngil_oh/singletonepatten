using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingleTonePattern
{
    public class GameController : MonoBehaviour
    {
        // Git TEST
        public void TestcSharpSingleton()
        {
            Debug.Log("C#");

            SingleToneCSharp instance = SingleToneCSharp.Instance;
            instance.TestSingleton();

            SingleToneCSharp instance2 = SingleToneCSharp.Instance;
            instance2.TestSingleton();
        }

        public void TestUnitySingleton()
        {
            Debug.Log("Unity");

            SingletonUnity instance = SingletonUnity.Instance;
            instance.TestSingleton();

            SingletonUnity instance2 = SingletonUnity.Instance;
            instance2.TestSingleton();
        }
    }
}


